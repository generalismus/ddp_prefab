# DDP_prefab

Prefab models for Unity.

## Getting started

- Place the `.prefab` and `.prefab.meta` files in the `"Unity-Project"/Assets/Prefab` folder.
- Place the `.mat` and `.mat.meta` files in the `"Unity-Project"/Assets/Materials` folder. 

- In Unity drag the prefabs into your Scene Hierachy.
